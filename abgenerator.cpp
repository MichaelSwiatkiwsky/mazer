/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "abgenerator.h"

ABGenerator::ABGenerator(Maze & _m, int _seed)  noexcept
	: Generator( _m, _seed)
{
}

//Chooses a random starting cell. Then choose random neighbour and create
//an edge if that cell hasn't been visited. Repeat until all cell are visited.
bool ABGenerator::mazeGen(void)  noexcept
{
	init();

	//Create and initialise grid for keeping track
	//of visited cells. NOTE: I could have instead
	//used a bool member attribute on each cell,
	//but that information is only relevant in this
	//function, so I decided to keep it here only.
	std::vector<std::vector<bool>> visited;
	for (int i = 0; i < HEIGHT; i++)
	{
		visited.push_back(std::vector<bool>());
		for (int j = 0; j < WIDTH; j++)
		{
			visited[i].push_back(bool(false));
		}

	}

	//Random coordinates
	int currentX = randInt(0, HEIGHT);
	int currentY = randInt(0, WIDTH);
	int targetX, targetY;

	//Start at random cell and mark visited
	Cell current = cells[currentX][currentY];
	visited[currentX][currentY] = true;

	std::vector<Cell*> neighbours;

	//We know in an acyclic maze, the number of edges will always be
	//(width * height) - 1
	while (myMaze.getnumEdges() < (WIDTH * HEIGHT) - 1)
	{

		neighbours = current.getNeighbours();
		//Choose random neighbour
		Cell target = *neighbours[randInt(0, neighbours.size() - 1)];
		targetX = target.getX();
		targetY = target.getY();

		//If the chosen neighbour hasn't been visited, 
		//add an edge and mark it visited
		if (!visited[targetX][targetY])
		{
			visited[targetX][targetY] = true;
			myMaze.makeEdge(currentX, currentY, targetX, targetY);
		}

		//Regardless of whether it was already visited, move to it and continue
		current = target;
		currentX = targetX;
		currentY = targetY;
	}

	cleanup();
	return true;
}