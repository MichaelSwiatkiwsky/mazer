/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include <vector>
#include "cell.h"
#include "maze.h"
#include "timer.h"
#include <map>

const int START_X = 0;
const int START_Y = 0;

class Solver
{

protected:
	Maze& myMaze;
	const std::vector<cellVec> & cells;

	const Cell& startCell;
	const Cell& endCell;

	std::vector<Cell> path;
	std::vector<std::vector<Cell*>> visitedFrom;
	void init(void) noexcept;
	void cleanup(void) noexcept;
	virtual void pushCell(const Cell & cell) = 0;
	virtual Cell popCell(void) = 0;
	virtual bool isContainerEmpty(void) = 0;
public:
	Solver(Maze& m) noexcept;
	virtual bool mazeSolve(void) noexcept;
	
	
};

