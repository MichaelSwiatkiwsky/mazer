/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "maze.h"

//Maze objects have default width and height of 10
Maze::Maze(int width, int height) noexcept
	: width(width), height(height), numEdges(0), solved(false)
{
}

//Simple function to set new Maze dimensions
bool Maze::resize(int newWidth, int newHeight) noexcept
{
	width = newWidth;
	height = newHeight;

	return true;
}

//Initialises the maze by filling cells grid with new Cells and
//giving each cell a vector of pointers to their neighbours.
//This can't be in the constructor because the Maze dimensions must be
//known, but in this implementation the Maze is sometimes resized
//before initialisation.
bool Maze::init(void) noexcept
{
	int i, j;

	//Fill cell vector with initialised cells
	cells.resize(height);
	for (i = 0; i < height; i++)
	{
		for (j = 0; j < width; j++)
		{
			cells[i].push_back(Cell(i, j));
		}
	}

	//Give each cell a vector of pointers to its neighbours.
	//This can only be done when the maze's Cell vector is full
	//which is why this is a separate loop.
	for (i = 0; i < width; i++)
	{
		for (j = 0; j < height; j++)
		{
			cells[i][j].findNeighbours(cells);
		}
	}	

	return true;
}

//Takes a vector of ints and checks if it represents a valid maze
bool Maze::isValid(std::vector<int> data) const noexcept
{
	const int NUM_INTS_BEFORE_EDGES = 3;
	const int NUM_INTS_PER_EDGE = 4;

	int width, height, numEdges, i;

	//Must at least have width, height, and numEdges
	if (data.size() < 3)
		return false;

	width = data[0];
	height = data[1];
	numEdges = data[2];

	//Validate width, height, and numEdges
	if (width < MIN_WIDTH || height < MIN_HEIGHT || numEdges < 0)
		return false;

	//First 3 ints are width, height, and numEdges.
	//After that every edge is 4 extra ints.
	if ((int)data.size() != 
		(NUM_INTS_BEFORE_EDGES + (numEdges * NUM_INTS_PER_EDGE)))
		return false;

	//From here each int alternates between x and y coordinate
	for (i = 3; i < (int)data.size(); i++)
	{
		//Validate x coordinate
		if (data[i] > width - 1)
			return false;

		//Increment because of the alternation
		i++;

		//Validate y coordinate
		if (data[i] > height - 1)
			return false;
	}

	//If you've made it this far, maze is valid
	return true;
}

//Takes a string object as filename and attempts to fill an int vector by
//reading binary data from that file. Then that data is validated and used to
//set new Maze dimensions, initilise it, then populate the Edge vector.
bool Maze::loadBinary(std::string fileName) noexcept
{
	Timer::time();
	int i;
	//Open the file in binary read mode
	std::ifstream fileReader(fileName.c_str(),
		std::ios::in | std::ios::binary);

	if (fileReader)
	{
		std::cout << "Loading " << fileName << std::endl;
	}
	else
	{
		std::cout << "Unable to load " << fileName << std::endl;
		return false;
	}

	//Enable exception handling on the stream. Credit to Paul Miller for 
	//read/write exception handling.
	fileReader.exceptions(std::ios::badbit | std::ios::failbit);
	try
	{
		//Each iteration reads a single int and pushes it to data vector
		while (fileReader.read((char*)&i, sizeof(int)))
		{
			data.push_back(i);
		}

		//Close file no that we're finished with it
		fileReader.close();
	}
	catch (const std::ios_base::failure & f)
	{
		//we have reached the end of the file
		if (!(fileReader.rdstate() & std::ifstream::eofbit))
		{
			//there was an io error so display the exception message and return
			//false
			std::cerr << f.what() << std::endl;
			return false;
		}	
	}

	//Validate data
	if (!isValid(data))
	{
		std::cout << "Invalid .maze file." << std::endl;
		return false;
	}

	//New maze dimensions and number of edges
	this->data = data;
	resize(data[0], data[1]);
	numEdges = data[2];

	//Propogate error if initialisation fails
	if (!init())
		return false;

	//Fill edge vector with loaded edges
	for (i = 3; i < (int)data.size(); i++)
	{
		int Ax, Ay, Bx, By;

		Ax = data[i++];
		Ay = data[i++];
		Bx = data[i++];
		By = data[i]; //No need to incremement here since for loop will

		//edges.push_back(Edge(Ax, Ay, Bx, By));
		makeEdge(Ax, Ay, Bx, By);
	}
	std::cout << "Loading binary took " << Timer::time() 
		<< " seconds." << std::endl;
	return true;
}

//Takes a string object as filename and stores the maze data in binary form.
//If file does not exist, a new file is created.
bool Maze::saveBinary(std::string fileName) noexcept
{
	Timer::time();
	int i;

	//Open/create file for binary writing
	std::ofstream fileWriter(fileName.c_str(),
		std::ios::out | std::ios::binary);
	if (fileWriter)
	{
		std::cout << "Saving " << fileName << std::endl;
	}
	else
	{
		std::cout << "Unable to save " << fileName << std::endl;
		return false;
	}

	//Enable exception handling on the stream. Credit to Paul Miller for 
	//read/write exception handling.
	fileWriter.exceptions(std::ios::badbit | std::ios::failbit);

	try
	{
		//First three values should be width, height, and number of edges
		fileWriter.write((char*)&width, sizeof(int));
		fileWriter.write((char*)&height, sizeof(int));
		fileWriter.write((char*)&numEdges, sizeof(int));

		//Write each edge as a set of four coordinates
		BOOST_FOREACH(Edge e, edges)
		{
			i = e.getAx();
			fileWriter.write((char*)&i, sizeof(int));

			i = e.getAy();
			fileWriter.write((char*)&i, sizeof(int));

			i = e.getBx();
			fileWriter.write((char*)&i, sizeof(int));

			i = e.getBy();
			fileWriter.write((char*)&i, sizeof(int));
		}

		//Close file now that we're done
		fileWriter.close();
	}
	catch (const std::ios_base::failure& f)
	{
		//exception occured so output it and return false
		std::cerr << f.what() << std::endl;
		return false;
	}

	std::cout << "Saving binary took " << Timer::time() 
		<< " seconds." << std::endl;
	return true;
}

//Takes a string object as filename and creates an SVG visualisation of the
//maze. If file does not exist, a new file is created.
bool Maze::saveSVG(std::string fileName) noexcept
{
	Timer::time();
	const int VIEWBOX_WIDTH = 500;
	const int VIEWBOX_HEIGHT = 500;
	const int BACKGROUND_WIDTH = width * 5;
	const int BACKGROUND_HEIGHT = height * 5;
	//SCALE_FACTOR can be played around with according to preference
	const float SCALE_FACTOR = 0.08f;
	//STROKE_WIDTH needs to be dynamic based on size of maze
	const float STROKE_WIDTH =
		((BACKGROUND_WIDTH + BACKGROUND_HEIGHT) / float(2))
		* (1 / float(edges.size())) * SCALE_FACTOR;
	//This determines how wide the solution path will be compared
	//to the edge width.
	const float PATH_WIDTH_SCALE = 0.75;

	//Open/create file for writing SVG
	std::ofstream fileWriter(fileName.c_str());
	if (fileWriter)
	{
		std::cout << "Saving " << fileName << std::endl;
	}
	else
	{
		std::cout << "Unable to save " << fileName << std::endl;
		return false;
	}

	//Enable exception handling on the stream. Credit to Paul Miller for 
	//read/write exception handling.
	fileWriter.exceptions(std::ios::badbit | std::ios::failbit);

	try
	{
		//Opening tag
		fileWriter << "<svg viewBox=\'0 0 1 1\' width=\'"
			<< VIEWBOX_WIDTH
			<< "\' height=\'"
			<< VIEWBOX_HEIGHT
			<< "\' xmlns = \'http://www.w3.org/2000/svg\'>" << std::endl;

		//Background tag
		fileWriter << "<rect width=\'"
			<< BACKGROUND_WIDTH
			<< "\' height=\'"
			<< BACKGROUND_HEIGHT
			<< "\' style=\'fill: black\' />" << std::endl;

		//Creates a white stroke to represent each edge. The edge coordinates
		//are divided by maze dimensions to give a relative position.
		BOOST_FOREACH(Edge e, edges)
		{
			fileWriter << "<line stroke=\'white\' stroke-width=\'"
				<< STROKE_WIDTH
				<< "\' x1=\'"
				<< float(e.getAy()) / height
				<< "\' y1=\'"
				<< float(e.getAx()) / width
				<< "\' x2=\'"
				<< float(e.getBy()) / height
				<< "\' y2=\'"
				<< float(e.getBx()) / width
				<< "\'/>" << std::endl;
		}

		//If the maze has been solved, draw a red line for each edge in path
		if (solved)
		{
			//path.size()-1 because we don't process the final cell in path
			for (int i = 0; i < (int)path.size() - 1; i++)
			{
				fileWriter << "<line stroke=\'red\' stroke-width=\'"
					<< STROKE_WIDTH * PATH_WIDTH_SCALE
					<< "\' x1=\'"
					<< float(path[i].getY()) / height
					<< "\' y1=\'"
					<< float(path[i].getX()) / width
					<< "\' x2=\'"
					<< float(path[i + 1].getY()) / height
					<< "\' y2=\'"
					<< float(path[i + 1].getX()) / width
					<< "\'/>" << std::endl;
			}
		}


		//Closing tag
		fileWriter << "</svg>";

		//Close file now that we're done
		fileWriter.close();

	}
	catch (const std::ios_base::failure& f)
	{
		//exception occured so output it and return false
		std::cerr << f.what() << std::endl;
		return false;
	}

	std::cout << "Saving SVG took " << Timer::time()
		<< " seconds." << std::endl;
	return true;
}

int Maze::getWidth(void) const noexcept
{
	return width;
}

int Maze::getHeight(void) const noexcept
{
	return height;
}

int Maze::getnumEdges(void) const noexcept
{
	return numEdges;
}

const std::vector<cellVec>& Maze::getCells(void) const noexcept
{
	return cells;
}

//Takes the x and y coordinates of two cells and constructs a new edge
//which is then added to the edge vector.
void Maze::makeEdge(int Ax, int Ay, int Bx, int By) noexcept
{
	Cell& cellA = cells[Ax][Ay];
	Cell& cellB = cells[Bx][By];

	cellA.connect(&cellB);
	cellB.connect(&cellA);
	edges.push_back(Edge(Ax, Ay, Bx, By));
	numEdges++;
}

//Set the solution path and mark maze as solved
void Maze::setPath(std::vector<Cell>& p) noexcept
{
	path = p;
	solved = true;
}
