/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include "solver.h"
#include <stack>
class DFSSolver :
	public Solver
{
	std::stack<Cell> cellStack;

	void pushCell(const Cell & cell) noexcept override;
	Cell popCell(void) noexcept override;
	bool isContainerEmpty(void) noexcept override;
public:
	DFSSolver(Maze& m) noexcept;

};

