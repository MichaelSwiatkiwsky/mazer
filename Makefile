########################################################################
# Mazer - 2016
# By Michael Swiatkiwsky
# Makefile template created by Paul Miller
########################################################################

SOURCES=abgenerator.cpp action.cpp actionfactory.cpp bfssolver.cpp cell.cpp dfssolver.cpp dijkstracell.cpp dijkstraeuclideansolver.cpp dijkstramanhattansolver.cpp dijkstrasolver.cpp edge.cpp ellergenerator.cpp generator.cpp generatorfactory.cpp main.cpp maze.cpp solver.cpp solverfactory.cpp timer.cpp
HEADERS=abgenerator.h action.h actionfactory.h bfssolver.h binaryheap.h cell.h dfssolver.h dijkstracell.h dijkstraeuclideansolver.h dijkstramanhattansolver.h dijkstrasolver.h edge.h ellergenerator.h generator.h generatorfactory.h maze.h solver.h solverfactory.h timer.h
OBJECTS=abgenerator.o action.o actionfactory.o bfssolver.o cell.o dfssolver.o dijkstracell.o dijkstraeuclideansolver.o dijkstramanhattansolver.o dijkstrasolver.o edge.o ellergenerator.o generator.o generatorfactory.o main.o maze.o solver.o solverfactory.o timer.o
README=readme
MAKEFILE=Makefile
LFLAGS=-lboost_system -lboost_program_options
CFLAGS=-Wall -Wextra -pedantic -std=c++14
CC=g++

all:mazer


mazer: $(OBJECTS)
	$(CC) $(LFLAGS) $(OBJECTS) -o mazer

%.o:%.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c  $<		

#archive:
#	zip $(USER)-a1 $(SOURCES) $(HEADERS) $(README) $(MAKEFILE)

.PHONY:clean	
clean:
	rm -f *.o mazer	
