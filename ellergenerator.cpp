/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "ellergenerator.h"

EllerGenerator::EllerGenerator(Maze & _m, int _seed) noexcept
	: Generator(_m, _seed)
{
}

//Generates a maze using Eller's algorithm
bool EllerGenerator::mazeGen(void) noexcept
{
	init();

	//First assign each cell in first row to its own set
	for (nextSetNum = 0; nextSetNum < WIDTH; nextSetNum++)
	{
		sets.insert({ nextSetNum, std::vector<Cell>() });
		sets[nextSetNum].push_back(cells[0][nextSetNum]);
		setNumbers.insert( { cells[0][nextSetNum], nextSetNum } );
	}
	
	//For each cell in row, check if it's in same set as cell to the right.
	//If it isn't, randomly decide whether or not to create and edge and
	//merge sets.
	//WIDTH - 1 because we don't check last cell
	for (currRowNum = 0; currRowNum < HEIGHT; currRowNum++)
	{

		for (int j = 0; j < WIDTH - 1; j++)
		{
			//Check if cell to the right is in a different set
			if (setNumbers[cells[currRowNum][j]] !=
				setNumbers[cells[currRowNum][j + 1]])
			{
				//Coinflip to decide whether to connect. If final row, 
				//connect regardless of coinflip.
				if (coinFlip() || currRowNum == HEIGHT-1)
				{
					//Make edge and merge sets
					myMaze.makeEdge(currRowNum, j, currRowNum, j + 1);
					mergeSets(setNumbers[cells[currRowNum][j]],
						setNumbers[cells[currRowNum][j + 1]]);

				}
			}
		}
		//Skip the vertical connections step if at bottom row
		if (currRowNum == (HEIGHT - 1))
			break;

		//To hold list of cells that will connect downwards
		std::vector<Cell> verticleCells;

		//For each set, randomly determine which cells connect
		//downwards, ensuring at least one does.
		for (auto s : sets)
		{
			int numVerticles = 0;
			for (auto c : s.second)
			{
				//Flip a coin, or if it is last cell in set and no cells 
				//from that set are connecting downwards yet, add it anyway
				if (coinFlip() || (c == s.second.back() && numVerticles == 0))
				{
					numVerticles++;
					verticleCells.push_back(c);
				}
			}
		}

		//Now that we have a list of cells to connect downwards, do it
		for (Cell c : verticleCells)
		{
			connectDownwards(c);
		}

		//Iterate through cells in row below current
		for (auto c : cells[currRowNum + 1])
		{
			//If the cell is not in the map (has no set), make new set for it
			if (nextSetNumbers.find(c) == nextSetNumbers.end())
			{
				nextSetNumbers.insert( { c, nextSetNum} );
				nextSets.insert({ nextSetNum, std::vector<Cell>({c}) });
				nextSetNum++;
			}

		}

		//Move on to the next row. We only need to keep track of current row
		sets = nextSets;
		setNumbers = nextSetNumbers;
		nextSets.clear();
		nextSetNumbers.clear();
	}

	cleanup();
	return true;
}



//Takes two set numbers and merges those sets together
void EllerGenerator::mergeSets(int setANum, int setBNum) noexcept
{
	std::vector<Cell>& setA = sets[setANum];
	std::vector<Cell>& setB = sets[setBNum];

	//Update setNumbers map by replacing all setB numbers with setA numbers
	for (int j = 0; j < WIDTH; j++)
	{
		Cell cell = cells[currRowNum][j];

		if (setNumbers[cell] == setBNum)
		{
			setNumbers[cell] = setANum;
		}
	}

	//Add all cells in setB to setA, then remove setB from the sets map.
	setA.insert(setA.end(), std::make_move_iterator(setB.begin()), 
		std::make_move_iterator(setB.end()));
	//sets[setB].clear(); //Unsure if this improves or decreases performance
	sets.erase(setBNum);
}

//Takes a cell and connects it to the cell below it. Also adds the below
//cell to the same set as parameter cell, but using the nextSets and 
//nextSetNumbers map.
void EllerGenerator::connectDownwards(Cell cell) noexcept
{
	int Ax = cell.getX();
	int Ay = cell.getY();
	int Bx = Ax + 1;
	int By = Ay;
	int setNum = setNumbers[cell];
	Cell targetCell = cells[Bx][By];

	//Makes the edge
	myMaze.makeEdge(Ax, Ay, Bx, By);

	//Adds target cell to set and updates its set number
	nextSetNumbers.insert({ targetCell, setNum });
	nextSets.insert({ setNum, std::vector<Cell>() });
	nextSets[setNum].push_back(targetCell);
}

