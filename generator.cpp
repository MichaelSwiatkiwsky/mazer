/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "generator.h"

Generator::Generator(Maze & m, int _seed) noexcept
	: randGen(_seed), myMaze(m), seed(_seed),
	WIDTH(myMaze.getWidth()), HEIGHT(myMaze.getHeight()),
	cells(myMaze.getCells())
{
}

//Takes an mt19937 object, a int lower bound and an int upper bound.
//Returns a random int between and including those bounds.
int Generator::randInt(int x, int y) noexcept
{
	std::uniform_int_distribution<int> dist(x, y);
	return dist(randGen);
}

bool Generator::coinFlip(void) noexcept
{
	//return static_cast<bool>(randInt(0,1));
	//I used to just have the above line of code but that gives compile warning
	if ((randInt(0, 1) == 1))
	{
		return true;
	}
	else
	{
		return false;
	}
}

//Common initialisation for all generators
void Generator::init(void) noexcept
{
	Timer::time();
	std::cout << "Generating with seed: " << seed << std::endl;
	myMaze.init();
}

//Common finalisation for all generators
void Generator::cleanup(void) noexcept
{
	std::cout << "Generation took " << Timer::time()
		<< " seconds." << std::endl;
}