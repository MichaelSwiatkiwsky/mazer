/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "timer.h"

Timer::Timer() noexcept
{
}

//Returns the number of seconds since previous call of this function
double Timer::time(void) noexcept
{
	std::chrono::duration<double> elapsedSeconds;
	
	std::chrono::time_point<std::chrono::system_clock> currentTime 
		= std::chrono::system_clock::now();
	
	elapsedSeconds = currentTime - previousTime;
	previousTime = currentTime;
	return elapsedSeconds.count();
}

//Must initialise previous time here because it is static
std::chrono::time_point<std::chrono::system_clock> Timer::previousTime 
	= std::chrono::system_clock::now();