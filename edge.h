/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include "cell.h"
class Edge
{
	int Ax, Ay, Bx, By;
public:
	Edge(int Ax, int Ay, int Bx, int By) noexcept;
	Edge(Cell A, Cell B) noexcept;
	int getAx(void) const noexcept;
	int getAy(void) const noexcept;
	int getBx(void) const noexcept;
	int getBy(void) const noexcept;
};