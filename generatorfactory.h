/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include <string>
#include "generator.h"
#include "abgenerator.h"
#include "ellergenerator.h"
#include <memory>
class GeneratorFactory
{
public:
	GeneratorFactory() noexcept;
	static std::unique_ptr<Generator> makeGenerator
		(const std::string & genType, Maze& m, int seed) noexcept;
};

