/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "generatorfactory.h"

GeneratorFactory::GeneratorFactory() noexcept
{
}

//Uses the genType to return the appropriate generator object
std::unique_ptr<Generator> GeneratorFactory::makeGenerator
	(const std::string & genType, Maze & m, int seed) noexcept
{
	if (genType == "ga")
	{
		return std::unique_ptr<Generator>(new ABGenerator(m, seed));
	}
	else if (genType == "ge")
	{
		return std::unique_ptr<Generator>(new EllerGenerator(m, seed));
	}

	return nullptr;
}
