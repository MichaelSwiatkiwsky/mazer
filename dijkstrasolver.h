/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include "solver.h"
#include "binaryheap.h"
#include "dijkstracell.h"
class DijkstraSolver :
	public Solver
{
protected:
	BinaryHeap<DijkstraCell> cellHeap;

	void pushCell(const Cell & cell) noexcept;
	Cell popCell(void) noexcept;
	bool isContainerEmpty(void) noexcept;
	virtual double calculateValue(const Cell & cell) = 0;
public:
	DijkstraSolver(Maze& m) noexcept;
};

