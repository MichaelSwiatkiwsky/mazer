/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include "generator.h"
class ABGenerator :
	public Generator
{
public:
	ABGenerator(Maze & _m, int _seed) noexcept;
	bool mazeGen(void) noexcept override;
};

