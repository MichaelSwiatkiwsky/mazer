/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "bfssolver.h"

BFSSolver::BFSSolver(Maze& m) noexcept
	: Solver(m)
{
}

//Adds a cell to the solver container
void BFSSolver::pushCell(const Cell & cell) noexcept
{
	cellQueue.push(cell);
}

//Removes next cell from solver container and returns it
Cell BFSSolver::popCell(void) noexcept
{
	Cell currCell = cellQueue.front();
	cellQueue.pop();
	return currCell;
}

//Returns true if container is empty
bool BFSSolver::isContainerEmpty(void) noexcept
{
	return cellQueue.empty();
}


