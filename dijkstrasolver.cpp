/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "dijkstrasolver.h"

DijkstraSolver::DijkstraSolver(Maze& m) noexcept
	: Solver(m)
{
}

//Adds a cell to the solver container
void DijkstraSolver::pushCell(const Cell & cell) noexcept
{
	cellHeap.insert(DijkstraCell(cell, calculateValue(cell)));

}

//Removes next cell from solver container and returns it
Cell DijkstraSolver::popCell(void) noexcept
{
	return cellHeap.pop();
}

//Returns true if container is empty
bool DijkstraSolver::isContainerEmpty(void) noexcept
{
	return cellHeap.empty();
}
