/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include "action.h"
#include <memory>

class ActionFactory
{
public:
	ActionFactory() noexcept;
	static std::unique_ptr<Action> makeAction(std::string optionType,
		std::vector<std::string> options,
		std::shared_ptr<Maze>* maze) noexcept;
};

