/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include <chrono>
class Timer
{
	static std::chrono::time_point<std::chrono::system_clock> previousTime;
public:
	Timer() noexcept;
	static double time(void) noexcept;
};

