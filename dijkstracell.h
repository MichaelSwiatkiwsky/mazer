/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include "cell.h"
class DijkstraCell :
	public Cell
{
	double value;
public:
	DijkstraCell(int x, int y, double v) noexcept;
	DijkstraCell(const Cell& c, double v) noexcept;
	bool operator< (const DijkstraCell& rhs) const noexcept;
	double getValue(void) const noexcept ;
};

