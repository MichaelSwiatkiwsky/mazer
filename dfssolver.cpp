/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "dfssolver.h"

DFSSolver::DFSSolver(Maze& m) noexcept
	: Solver(m)
{
}

//Adds a cell to the solver container
void DFSSolver::pushCell(const Cell & cell) noexcept
{
	cellStack.push(cell);
}

//Removes next cell from solver container and returns it
Cell DFSSolver::popCell(void) noexcept
{
	Cell currCell = cellStack.top();
	cellStack.pop();
	return currCell;
}

//Returns true if container is empty
bool DFSSolver::isContainerEmpty(void) noexcept
{
	return cellStack.empty();
}