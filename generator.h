/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once

#include <random>
#include "maze.h"
#include "cell.h"
#include "timer.h"

class Generator
{
	std::mt19937 randGen;

protected:
	Maze & myMaze;
	int seed;
	const int WIDTH;
	const int HEIGHT;
	const std::vector<cellVec> & cells;

	int randInt(int x, int y) noexcept;
	bool coinFlip(void) noexcept;
	void init(void) noexcept;
	void cleanup(void) noexcept;

public:
	Generator(Maze & m, int seed) noexcept;
	virtual bool mazeGen(void) = 0;
};

