/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include <iostream>
#include <string>
#include "maze.h"
#include <boost/program_options.hpp>
#include <chrono>
#include "abgenerator.h"
#include "ellergenerator.h"
#include "bfssolver.h"
#include "dfssolver.h"
#include "dijkstramanhattansolver.h"
#include "dijkstraeuclideansolver.h"
#include "actionfactory.h"

namespace po = boost::program_options;

int main(int argc, char* argv[])
{
	//Variables to store command line options
	std::vector<int> genArgs;
	std::vector<std::string> loadFile;
	std::vector<std::string> saveBinaryFile;
	std::vector<std::string> saveSVGFile;

	//Pointer to the current maze on heap
	std::shared_ptr<Maze> myMaze = nullptr;

	//Initialise command line parser with address of option variables
	po::options_description desc("Allowed options");
	desc.add_options()
		("ga", po::value< std::vector<int> >(&genArgs)->multitoken(), "gen")
		("ge", po::value< std::vector<int> >(&genArgs)->multitoken(), "gen")
		("lb", po::value<std::vector<std::string>>(&loadFile), "loadbin")
		("sb", po::value<std::vector<std::string>>(&saveBinaryFile), "savebin")
		("sv", po::value<std::vector<std::string>>(&saveSVGFile), "saveSVG")
		("pb", "solve BFS")
		("pd", "solve DFS")
		("pm", "solve dijkstra manhattan")
		("pe", "solve dijkstra euclidean");

	po::variables_map vm;

	//Parse command line and catch invalid argument exception
	try
	{
		auto parsed = po::parse_command_line(argc, argv, desc);
		po::store(parsed, vm);
		po::notify(vm);

		//This is the main program control. Loop for all options the user
		//has selected and for each use the ActionFactory to create the 
		//appropriate action functor. Then execute the functor and abort
		//the program if something went wrong.
		for (auto &opt : parsed.options) 
		{
			auto optionType = opt.string_key;
			std::unique_ptr<Action> a = 
				ActionFactory::makeAction(optionType, opt.value, &myMaze);
			if(!(*a)())
				return 1;
		}

	}
	//Catch invalid argument exceptions
	catch (po::error& e)
	{
		std::cout << e.what() << std::endl;
		return 1;
	}
	//I have never seen this trigger but it is here for safety
	catch (...)
	{
		std::cout << "Unknown exception." << std::endl;
		return 1;
	}


	//Program Exit
	return 0;
}