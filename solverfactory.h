/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include <memory>
#include <string>
#include "bfssolver.h"
#include "dfssolver.h"
#include "dijkstraeuclideansolver.h"
#include "dijkstramanhattansolver.h"
class SolverFactory
{
public:
	SolverFactory() noexcept;
	static std::unique_ptr<Solver> makeSolver
		(const std::string & solveType, Maze& m) noexcept;
};

