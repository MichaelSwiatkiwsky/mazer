/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "action.h"

Action::Action() noexcept
{
}

Action::Action(std::string optionType, std::vector<std::string> options,
	std::shared_ptr<Maze>* maze)  noexcept
	: myOptionType(optionType), myValues(options), myMaze(maze)
{
}

invalidAction::invalidAction()  noexcept
{
}

bool invalidAction::operator()(void)  noexcept
{
	std::cout << "Invalid action." << std::endl;
	return false;
}

generate::generate(std::string optionType, std::vector<std::string> options,
	std::shared_ptr<Maze>* maze) noexcept
	: Action(optionType, options, maze)
{
}

//Generates a new maze polymorphically
bool generate::operator()(void) noexcept
{
	int seed;

	int numGenArgs = myValues.size();
	if (numGenArgs == 1)
	{
		//Generate maze with given seed and default width/height
		seed = stoi(myValues[0]);
		*myMaze = std::shared_ptr<Maze>(new Maze);	
	}
	else if (numGenArgs == 2)
	{
		//Generate maze with random seed and given width/height
		seed = (int)std::chrono::high_resolution_clock::now()
			.time_since_epoch().count();
		*myMaze = std::shared_ptr<Maze>
			(new Maze(stoi(myValues[0]), stoi(myValues[1])));
	}
	else if (numGenArgs == 3)
	{
		//Generate maze with given seed and given width/height
		seed = stoi(myValues[0]);
		*myMaze = std::shared_ptr<Maze>
			(new Maze(stoi(myValues[0]), stoi(myValues[1])));
	}
	else
	{
		std::cout << "Too many --g options" << std::endl;
		return false;
	}

	auto myGen = GeneratorFactory::makeGenerator(myOptionType, **myMaze, seed);
	return (*myGen).mazeGen();
}

solve::solve(std::string optionType, std::vector<std::string> options,
	std::shared_ptr<Maze>* maze) noexcept
	: Action(optionType, options, maze)
{
}

//Solves an existing maze polymorphically
bool solve::operator()(void)  noexcept
{
	if (*myMaze == nullptr)
	{
		std::cout << "Cannot solve maze because no maze exists." << std::endl;
		return false;
	}
	else
	{
		auto mySolver = SolverFactory::makeSolver(myOptionType, **myMaze);
		return (*mySolver).mazeSolve();
	}
}

loadBinary::loadBinary(std::string optionType, 
	std::vector<std::string> options, std::shared_ptr<Maze>* maze) noexcept
	: Action(optionType, options, maze)
{
}

//Loads a new maze from a binary file
bool loadBinary::operator()(void) noexcept
{
	(*myMaze) = std::shared_ptr<Maze>(new Maze);
	
	return (**myMaze).loadBinary(myValues[0]);
}

saveBinary::saveBinary(std::string optionType,
	std::vector<std::string> options, std::shared_ptr<Maze>* maze) noexcept
	: Action(optionType, options, maze)
{
}

//Saves the maze to binary
bool saveBinary::operator()(void) noexcept
{
	if (*myMaze == nullptr)
	{
		std::cout << "Cannot save binary because no maze exists." << std::endl;
		return false;
	}
	else
	{
		return (**myMaze).saveBinary(myValues[0]);
	}
}

saveSVG::saveSVG(std::string optionType, std::vector<std::string> options,
	std::shared_ptr<Maze>* maze) noexcept
	: Action(optionType, options, maze)
{
}

//Saves the SVG of an existing maze, and draws the path in red
//if maze has been solved.
bool saveSVG::operator()(void) noexcept
{
	if (*myMaze == nullptr)
	{
		std::cout << "Cannot save SVG because no maze exists." << std::endl;
		return false;
	}
	else
	{
		return (**myMaze).saveSVG(myValues[0]);
	}
	
}
