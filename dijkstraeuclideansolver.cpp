/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "dijkstraeuclideansolver.h"

DijkstraEuclideanSolver::DijkstraEuclideanSolver(Maze& m) noexcept
	: DijkstraSolver(m)
{
}

//Uses trigonometry to calculate the distance of a straight line 
//between two cells.
double DijkstraEuclideanSolver::calculateValue(const Cell & cell) noexcept
{
	int Ax = cell.getX();
	int Ay = cell.getY();
	int Bx = endCell.getX();
	int By = endCell.getY();

	double ret = static_cast<double>
		(sqrt(pow(abs(Ax - Bx), 2) + pow(abs(Ay - By), 2)));

	return ret;
}


