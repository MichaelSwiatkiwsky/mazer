/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include <string>
#include "maze.h"
#include "solver.h"
#include "generator.h"
#include <vector>
#include <memory>
#include "abgenerator.h"
#include "ellergenerator.h"
#include "bfssolver.h"
#include "dfssolver.h"
#include "dijkstramanhattansolver.h"
#include "dijkstraeuclideansolver.h"
#include "generatorfactory.h"
#include "solverfactory.h"

class Action
{
protected:
	std::string myOptionType;
	std::vector<std::string> myValues;
	std::shared_ptr<Maze>* myMaze;

public:
	Action() noexcept;
	Action(std::string optionType, std::vector<std::string> options, 
		std::shared_ptr<Maze>* maze) noexcept;
	virtual bool operator() (void) = 0;
};

class invalidAction :
	public Action
{
public:
	invalidAction() noexcept;
	bool operator() (void) noexcept override;
};

class generate :
	public Action
{
public:
	generate(std::string optionType, std::vector<std::string> options,
		std::shared_ptr<Maze>* maze) noexcept;
	bool operator() (void) noexcept override;
};

class solve :
	public Action
{
public:
	solve(std::string optionType, std::vector<std::string> options,
		std::shared_ptr<Maze>* maze) noexcept;
	bool operator() (void) noexcept override;
};

class loadBinary :
	public Action
{
public:
	loadBinary(std::string optionType, std::vector<std::string> options,
		std::shared_ptr<Maze>* maze) noexcept;
	bool operator() (void) noexcept override;
};

class saveBinary :
	public Action
{
public:
	saveBinary(std::string optionType, std::vector<std::string> options,
		std::shared_ptr<Maze>* maze) noexcept;
	bool operator() (void) noexcept override;
};

class saveSVG :
	public Action
{
public:
	saveSVG(std::string optionType, std::vector<std::string> options,
		std::shared_ptr<Maze>* maze) noexcept;
	bool operator() (void) noexcept override;
};





