/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "solverfactory.h"

SolverFactory::SolverFactory() noexcept
{
}

//Uses solveType to return the appropriate solver object
std::unique_ptr<Solver> SolverFactory::makeSolver
	(const std::string & solveType, Maze & m) noexcept
{

	if (solveType == "pd")
	{
		return std::unique_ptr<Solver>(new DFSSolver(m));
	}
	else if (solveType == "pb")
	{
		return std::unique_ptr<Solver>(new BFSSolver(m));
	}
	else if (solveType == "pm")
	{
		return std::unique_ptr<Solver>(new DijkstraManhattanSolver(m));
	}
	else if (solveType == "pe")
	{
		return std::unique_ptr<Solver>(new DijkstraEuclideanSolver(m));
	}

	return nullptr;
}
