/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "dijkstracell.h"

DijkstraCell::DijkstraCell(int x, int y, double v) noexcept
	: Cell(x,y), value(v)
{
}

//Takes a regular cell plus a value and constructs a DijkstraCell that holds
//that value as well as x,y coordinates.
DijkstraCell::DijkstraCell(const Cell & c, double v) noexcept
	: Cell(c), value(v)
{
}

//DijkstraCells use their special value member attribute to determine
//whether another DijkstraCell is less than it.
bool DijkstraCell::operator<(const DijkstraCell & rhs) const noexcept
{
	if (value < rhs.getValue())
		return true;
	else
		return false;
}

double DijkstraCell::getValue(void) const noexcept
{
	return value;
}
