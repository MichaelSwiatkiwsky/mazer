/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include <vector>
#include "cell.h"
#include "edge.h"
#include <boost/foreach.hpp>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include "timer.h"

const int MIN_WIDTH = 4;
const int MIN_HEIGHT = 4;

class Maze
{
	int width;
	int height;
	int numEdges;

	std::vector<int> data;
	std::vector<cellVec> cells;
	std::vector<Edge> edges;

	bool solved;
	std::vector<Cell> path;


public:

	Maze(int width = 10, int height = 10) noexcept;
	bool resize(int newWidth, int newHeight) noexcept;
	bool init(void) noexcept;
	bool isValid(std::vector<int> data) const noexcept;
	bool loadBinary(std::string fileName) noexcept;
	bool saveBinary(std::string fileName) noexcept;
	bool saveSVG(std::string fileName) noexcept;

	int getWidth(void) const noexcept;
	int getHeight(void) const noexcept;
	int getnumEdges(void) const noexcept;

	const std::vector<cellVec> & getCells(void) const noexcept;
	void makeEdge(int Ax, int Ay, int Bx, int By) noexcept;
	void setPath(std::vector<Cell>& p) noexcept;

};