/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include "generator.h"
#include <vector>
#include <unordered_map>
#include <map>
#include <unordered_set>

class EllerGenerator :
	public Generator
{
	int currRowNum;
	int nextSetNum;

	//Map that associates set number with a vector of cells
	std::map<int, std::vector<Cell>> sets;
	std::map<int, std::vector<Cell>> nextSets;

	//Map to keep track of which set number each cell belongs to.
	//While this may seem like data duplication, it is necessary for faster
	//access time when checking what set a Cell belongs to. Otherwise
	//we would have to iterate through all our sets until we found it.
	std::map<Cell, int> setNumbers;
	std::map<Cell, int> nextSetNumbers;

	void mergeSets(int setA, int setB) noexcept;
	void connectDownwards(Cell) noexcept;
public:
	EllerGenerator(Maze & _m, int _seed) noexcept;
	bool mazeGen(void) noexcept override;
};
