/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

To compile the program simply run the makefile by using "make" in the same
directory as the source files. "make clean" can also be used to remove all
.o files along with the executable.

To run the program, there are a number of optional flags:

--ga [seed] [width] [height]
This flag generates a new maze with the given seed, width, and height. If only
two options are specified, seed will be randomly generated based off system
clock. If only one option is specified, width and height will both be 10 by 
default. Uses Aldous Broder generation algorithm.

--ge [seed] [width] [height]
This flag generates a new maze with the given seed, width, and height. If only
two options are specified, seed will be randomly generated based off system
clock. If only one option is specified, width and height will both be 10 by 
default. Uses Eller's generation algorithm.

--lb [loadfile]
Loads a maze from a valid .maze binary file.

--sb [binarysavefile]
Saves the maze to a binary file. Filename should use .maze extension.

--sv [SVGsavefile]
Saves maze to an SVG file for visualisation. Filename should use .svg 
extension. Will also save solution path in red if maze has been solved.

--pm
Solve maze using Dijkstra's algorithm and manhattan distance heuristic.

--pe
Solve maze using Dijkstra's algorithm and euclidean distance heuristic.

--pb
Solve maze using breadth first search.

--pd
Solve maze using depth first search