/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "dijkstramanhattansolver.h"

DijkstraManhattanSolver::DijkstraManhattanSolver(Maze& m) noexcept
	: DijkstraSolver(m)
{
}

//Calculates distance between two cells by summing vertical
//difference and horizontal difference.
double DijkstraManhattanSolver::calculateValue(const Cell & cell) noexcept
{
	int Ax = cell.getX();
	int Ay = cell.getY();
	int Bx = endCell.getX();
	int By = endCell.getY();

	int ret = abs(Ax - Bx) + abs(Ay - By);

	return static_cast<double>(ret);
}
