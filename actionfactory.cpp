/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "actionfactory.h"

ActionFactory::ActionFactory() noexcept
{
}

//Checks the option type and returns an appropriate action object
std::unique_ptr<Action> ActionFactory::makeAction(std::string optionType,
	std::vector<std::string> options, std::shared_ptr<Maze>* maze) noexcept
{
	if (optionType[0] == 'g')
	{
		return std::unique_ptr<Action>
			(new generate(optionType, options, maze));
	}
	else if (optionType[0] == 'p')
	{
		return std::unique_ptr<Action>
			(new solve(optionType, options, maze));
	}
	else if (optionType == "lb")
	{
		return std::unique_ptr<Action>
			(new loadBinary(optionType, options, maze));
	}
	else if (optionType == "sb")
	{
		return std::unique_ptr<Action>
			(new saveBinary(optionType, options, maze));
	}
	else if (optionType == "sv")
	{
		return std::unique_ptr<Action>
			(new saveSVG(optionType, options, maze));
	}
	else
		return std::unique_ptr<Action>
			(new invalidAction);
}
