/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include <vector>
template <typename T>
class BinaryHeap	
{
	//By applying this offset to all vector indices we can avoid wasting space
	//on a blank item at position 0. We can pretend it exists for the sake
	//of binary heap algorithms, but it is never used so it is never created.
	const int OFFSET = -1;

	std::vector<T> data;
	int size;

	//Takes indexes of two items in data vector and swaps them.
	void swapItems(int indexA, int indexB)
	{
		T temp = data[indexA];
		data[indexA] = data[indexB];
		data[indexB] = temp;
	}

public:

	BinaryHeap()
		: size(0)
	{
	}

	//Takes an item and inserts it into the binary heap while maintaining
	//minheap structure.
	void insert(T item)
	{
		//Initially insert at back of vector
		data.push_back(item);
		size++;
		int pos = size;
		int parentPos = pos / 2;

		//Percolates the inserted item towards the top until it is either at
		//the top, or its partent is less than it.
		while (parentPos > 0 && data[pos + OFFSET] < data[parentPos + OFFSET])
		{
			swapItems(pos + OFFSET, parentPos + OFFSET);
			pos = parentPos;
			parentPos /= 2;
		}
	}

	//Removes the first (smallest) value from vector and returns it. Then
	//repairs minheap structure.
	T pop(void)
	{
		//Save value to be returned
		T ret = data.front();
		int pos = 1;

		//Replace first item with item at the back
		data[0] = data.back();
		data.pop_back();
		size--;

		//No need to continue if there is only one item in vector
		if (size > 1)
		{		
			//We're going to loop until the minheap is repaired
			bool done = false;
			while (!done)
			{
				int childOnePos = pos * 2;
				int childTwoPos = (pos * 2) + 1;

				//If child 1 is in vector bounds and is less than parent.
				if (childOnePos <= size && data[childOnePos + OFFSET]
						< data[pos + OFFSET])
				{
					//If child 2 is in vector bounds and is less than child 1,
					//then child 2 must be smaller than parent, so swap
					//child 2 with parent.
					if (childTwoPos <= size && data[childTwoPos + OFFSET]
							< data[childOnePos + OFFSET])
					{
						swapItems(pos + OFFSET, childTwoPos + OFFSET);
						pos = childTwoPos;
					}
					//Otherwise we know that child 1 is only valid/smaller
					//child, so swap child 1 with parent.
					else
					{
						swapItems(pos + OFFSET, childOnePos + OFFSET);
						pos = childOnePos;
					}
				}
				//Here we already know child 1 is not valid/smaller, so if 
				//child 2 is in vector bounds and is less than parent,
				//swap child 2 with parent.
				else if (childTwoPos <= size && data[childTwoPos + OFFSET]
							< data[pos + OFFSET])
				{
					swapItems(pos + OFFSET, childTwoPos + OFFSET);
					pos = childTwoPos;
				}
				//Here we already know child 1 is not valid/smaller, so if 
				//child 2 is also not valid/smaller, we are done.
				else
				{
					done = true;
				}
			}
		}

		return ret;
	}

	//Returns true if binaryheap is empty, otherwise returns false
	bool empty(void) const
	{
		if (size == 0)
			return true;
		else
			return false;
	}
};

