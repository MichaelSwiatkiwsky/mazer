/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "edge.h"

//An Edge object abstractly represents two connected Cells
//A and B, by holding the x and y values of each. This is 
//better than storing two Cell references in terms of performance
//and also more closely resembles the way the data is stored in
//the binary file.
Edge::Edge(int Ax, int Ay, int Bx, int By) noexcept
	: Ax(Ax), Ay(Ay), Bx(Bx), By(By)
{
}

Edge::Edge(Cell A, Cell B) noexcept
	: Ax(A.getX()), Ay(A.getY()), Bx(B.getX()), By(B.getY())
{
}

int Edge::getAx(void) const noexcept
{
	return Ax;
}

int Edge::getAy(void) const noexcept
{
	return Ay;
}

int Edge::getBx(void) const noexcept
{
	return Bx;
}

int Edge::getBy(void) const noexcept
{
	return By;
}