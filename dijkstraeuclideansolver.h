/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include "dijkstrasolver.h"
#include <cmath>
class DijkstraEuclideanSolver :
	public DijkstraSolver
{
	double calculateValue(const Cell & cell) noexcept;
public:
	DijkstraEuclideanSolver(Maze& m) noexcept;
	
};

