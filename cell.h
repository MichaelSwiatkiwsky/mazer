/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include <vector>

class Cell;
typedef std::vector<Cell> cellVec;

class Cell
{
protected:
	int x, y;
	std::vector<Cell*> connectedNeighbours;
	std::vector<Cell*> neighbours;	
public:
	Cell(int x, int y) noexcept;
	Cell(const Cell& c) noexcept;
	bool operator== (const Cell& rhs) const noexcept;
	bool operator!= (const Cell& rhs) const noexcept;
	virtual bool operator< (const Cell& rhs) const noexcept;
	bool findNeighbours(const std::vector<cellVec>& cells) noexcept;
	const std::vector<Cell*>& getConnectedNeighbours(void) const noexcept;
	const std::vector<Cell*>& getNeighbours(void) const noexcept;
	int getX(void) const noexcept;
	int getY(void) const noexcept;
	void connect(Cell*) noexcept;
};