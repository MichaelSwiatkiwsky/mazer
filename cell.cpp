/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "cell.h"
#include <algorithm>

Cell::Cell(int x, int y) noexcept
	: x(x), y(y)
{
}

Cell::Cell(const Cell& c) noexcept
{
	x = c.x;
	y = c.y;
	neighbours = c.neighbours;
	connectedNeighbours = c.connectedNeighbours;
}

bool Cell::operator==(const Cell& rhs) const noexcept
{
	if (x == rhs.x && y == rhs.y)
		return true;
	else
		return false;
}

bool Cell::operator!=(const Cell & rhs) const noexcept
{
	return !(Cell::operator==(rhs));
}

bool Cell::operator<(const Cell & rhs) const noexcept
{
	if (x < rhs.x || (x == rhs.x && y < rhs.y))
		return true;
	else
		return false;
}

//Takes const ref of a vector of Cell vectors (ie, grid of Maze's cells)
//and uses that to add a pointer of each adjacent Cell to the
//Cell's neighbours vector. Implements bounds-checking.
bool Cell::findNeighbours(const std::vector<cellVec>& cells) noexcept
{
	//Above
	if (x - 1 >= 0)
		neighbours.push_back((Cell*)&cells[x - 1][y]);
	//Below
	if (x + 1 < (int)cells.size())
		neighbours.push_back((Cell*)&cells[x + 1][y]);
	//Left
	if (y - 1 >= 0)
		neighbours.push_back((Cell*)&cells[x][y - 1]);
	//Right
	if (y + 1 < (int)cells[x].size())
		neighbours.push_back((Cell*)&cells[x][y + 1]);

	return true;
}

const std::vector<Cell*>& Cell::getConnectedNeighbours(void) const noexcept
{
	return connectedNeighbours;
}

const std::vector<Cell*>& Cell::getNeighbours(void) const noexcept
{
	return neighbours;
}

int Cell::getX(void) const noexcept
{
	return x;
}

int Cell::getY(void) const noexcept
{
	return y;
}

//Takes a cell pointer and adds it to connectedNeighbours
void Cell::connect(Cell * cellp) noexcept
{
	connectedNeighbours.push_back(cellp);
}
