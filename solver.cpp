/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#include "solver.h"

Solver::Solver(Maze& m) noexcept
	: myMaze(m), cells(m.getCells()), 
	startCell(cells[START_X][START_Y]), 
	endCell(cells[m.getHeight()-1][m.getWidth() - 1])
{
}

//Common initialisation for all solvers
void Solver::init(void) noexcept
{
	Timer::time();

	visitedFrom.resize(myMaze.getHeight());
	for (auto & v : visitedFrom)
	{
		v.resize(myMaze.getWidth());
	}
}

//Common finalisation for all solvers
void Solver::cleanup(void) noexcept
{
	std::cout << "Solving took " << Timer::time() << " seconds." << std::endl;
}

//Solves the maze and generates a path from start cell to end cell.
//All solvers use this algorithm, but using different container types.
bool Solver::mazeSolve(void) noexcept
{
	init();

	//Add starting cell to queue
	pushCell(startCell);

	//Loop until container is empty. If container empties before
	//maze has been solved, no solution exists.
	while(!isContainerEmpty())
	{
		//Get the next cell in the container
		Cell currCell = popCell();

		//If we have solved the maze
		if (currCell == endCell)
		{
			//Use the grid of previous cell pointers to backtrack
			//all the way back to startCell
			while (visitedFrom[currCell.getX()][currCell.getY()] != nullptr)
			{
				//Add each cell processed this way to the solution path
				path.push_back(currCell);
				currCell = *visitedFrom[currCell.getX()][currCell.getY()];
			}

			//Add the final cell to path (should be startCell)
			path.push_back(currCell);

			//Set the maze path and exit
			myMaze.setPath(path);
			cleanup();
			return true;

		}
		else
		{
			auto connectedNeighbours = currCell.getConnectedNeighbours();
			//For each connected neighbour of current cell
			for (auto c : connectedNeighbours)
			{
				//Check if it is unvisited by checking if we have stored a 
				//visitedFrom cell pointer yet. Also make sure it is not
				//startCell because startCell does not have a visitedFrom
				if (visitedFrom[(*c).getX()][(*c).getY()]
					== nullptr && *c != startCell)
				{
					//Add the neighbour to the container fo cells 
					//to be processed and keep track of where we came from.
					pushCell(*c);
					visitedFrom[(*c).getX()][(*c).getY()]
						= (Cell*)&cells[currCell.getX()][currCell.getY()];
				}
			}
		}
	}

	//Should not reach here
	std::cout << "Maze not solvable." << std:: endl;
	return false;
}