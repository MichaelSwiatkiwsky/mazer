/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include "dijkstrasolver.h"
#include <cmath>
class DijkstraManhattanSolver :
	public DijkstraSolver
{
	double calculateValue(const Cell & cell) noexcept;
public:
	DijkstraManhattanSolver(Maze& m) noexcept;
	
};

