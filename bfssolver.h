/***************************************
* Mazer - 2016
* By Michael Swiatkiwsky
****************************************/

#pragma once
#include "solver.h"
#include <queue>
class BFSSolver :
	public Solver
{
	std::queue<Cell> cellQueue;

	void pushCell(const Cell & cell) noexcept override;
	Cell popCell(void) noexcept override;
	bool isContainerEmpty(void) noexcept override;

public:
	BFSSolver(Maze& m) noexcept;
};

